# transgit

[Latest Build](https://gitlab.com/linalinn/transgit/-/jobs/artifacts/main/browse?job=build)

[example config](https://gitlab.com/linalinn/transgit/-/blob/main/exsample-config.yaml)

## Providers
 * Gitlab (via rest api)
 * [Git (work in progress local fs)](https://gitlab.com/linalinn/transgit/-/tree/git-provider) 

## Transformations

### Regex
```yaml
- find: 'find regex'
  replace: 'replace'
  conditions:
    filename: regex.txt
```

### File
```yaml
- name: newOrUpdatedFile.txt
  path: 'files/newOrUpdatedFile.txt'
  content: 'New Content'
```
Currently, you can only create or update files.
Deleting files is not yet Implemented.