package providers

import (
	"transgit/pkg/utils"
)

// TODO: Add NewFile func and make hash not exported
type File struct {
	Name    string
	Path    string
	Hash    string
	Size    int
	Content string
}

func (f *File) IsModified() bool {
	return utils.Hash(f.Content) != f.Hash
}

type Tree struct {
	Name string
	Path string
	Type string
}

type UpdateFileOptions struct {
}

type CreateFileOptions struct {
}

type ReadOnlyProvider interface {
	GetFile(path string) (*File, error)
	GetTree() ([]*Tree, error)
}

type Provider interface {
	ReadOnlyProvider
	//UpdateFile(file *File, options *UpdateFileOptions)
	//DeleteFile(path string)
	CreateFile(file *File, options *CreateFileOptions)
	CommitAll()
	Close()
}
