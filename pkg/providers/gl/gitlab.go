package gl

import (
	"encoding/base64"
	"fmt"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/yaml.v3"
	"log"
	"transgit/pkg/providers"
	glApi "transgit/pkg/providers/gl/api"
	"transgit/pkg/utils"
)

type GitlabProvider struct {
	config         *GitlabProviderConfig
	client         glApi.GlApiInterface
	pid            interface{}
	ref            string
	defaultRef     string
	fileCache      map[string]*providers.File
	createdFiles   map[string]*providers.File
	mergeRequestID int
}

func NewGitlabProviderFromYamlNode(config yaml.Node) *GitlabProvider {
	var glConfig GitlabProviderConfig
	config.Decode(&glConfig)
	CheckConfigurationAndSetStaticDefaults(&glConfig)

	var client *gitlab.Client

	var err error
	if glConfig.Host != "" {
		client, err = gitlab.NewClient(glConfig.Token, gitlab.WithBaseURL(glConfig.Host))
	} else {
		client, err = gitlab.NewClient(glConfig.Token)
	}
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	api := glApi.GlApi{
		Client: client,
	}

	defaultBranch, err := api.GetDefaultBranch(glConfig.Project)
	if err != nil {
		panic(err)
	}

	branch := true
	var ref string
	var from string
	if glConfig.Branch.Name != "" {
		ref = glConfig.Branch.Name
	}
	switch {
	case glConfig.Branch.Use != "":
		branch = false
		ref = glConfig.Branch.Use
	case glConfig.Branch.From != "":
		from = glConfig.Branch.From
	case glConfig.Branch.From == "":
		from = defaultBranch.Name
	}

	if branch {
		newBranch, err := api.NewBranch(glConfig.Project, from, ref)
		if err != nil {
			log.Fatalf("Failed to create Branch")
			panic(err)
		}
		ref = newBranch.Name
	}

	mergeRequestID := 0
	if glConfig.MergeRequest != nil {
		target := glConfig.MergeRequest.Target
		if target == "" {
			target = defaultBranch.Name
		}
		mergeRequest, err := api.CreateMergeRequest(glConfig.Project,
			glConfig.MergeRequest.Title,
			glConfig.MergeRequest.Reviewer,
			ref,
			target)
		if err != nil {
			panic(err)
		}
		mergeRequestID = mergeRequest.ID
	}

	return &GitlabProvider{
		&glConfig,
		&api,
		glConfig.Project,
		ref,
		defaultBranch.Name,
		map[string]*providers.File{},
		map[string]*providers.File{},
		mergeRequestID,
	}

}

func (g *GitlabProvider) Close() {
	//TODO: Implement collected commit
	if g.config.MergeRequest != nil {
		switch g.config.MergeRequest.Merge {
		case "pipeline":
			g.client.MergeMergeRequest(g.pid, g.mergeRequestID, true)
		case "always":
			g.client.MergeMergeRequest(g.pid, g.mergeRequestID, false)

		}
	}
}

func (g *GitlabProvider) CreateFile(file *providers.File, options *providers.CreateFileOptions) {
	g.createdFiles[file.Path] = file

}

func (g *GitlabProvider) GetFile(path string) (*providers.File, error) {
	if val, ok := g.fileCache[path]; ok {
		fmt.Printf("Get file: %s from cache\n", val.Name)
		return val, nil
	}
	glFile, err := g.client.GetFile(g.pid, path, g.ref)
	if err != nil {
		return nil, err
	}
	file := fileMapper(glFile)
	g.fileCache[file.Path] = file
	return file, nil
}

func (g *GitlabProvider) GetTree() ([]*providers.Tree, error) {
	tree, err := g.client.ListTree(g.pid, g.ref)
	if err != nil {
		return nil, err
	}
	return treeMapper(tree), nil
}

func (g *GitlabProvider) CommitAll() {
	if g.config.Commit.Collect == false {
		for _, file := range g.fileCache {
			if file.IsModified() {
				g.client.CommitUpdatedFile(g.pid,
					g.ref,
					g.config.Commit.MessagePrefix+" updated file",
					g.config.Commit.User,
					g.config.Commit.Email,
					file)
			}
		}
		for _, file := range g.createdFiles {
			g.client.CommitCreatedFile(g.pid,
				g.ref,
				g.config.Commit.MessagePrefix+" created file",
				g.config.Commit.User,
				g.config.Commit.Email,
				file)
		}
	} else {
		panic("Collected Commits are not implemented yet")
	}

}

func fileMapper(file *gitlab.File) *providers.File {
	bString, _ := base64.StdEncoding.DecodeString(file.Content)
	return &providers.File{
		Name:    file.FileName,
		Hash:    utils.Hash(string(bString)),
		Path:    file.FilePath,
		Size:    file.Size,
		Content: string(bString),
	}
}

func treeMapper(node []*gitlab.TreeNode) []*providers.Tree {
	tree := []*providers.Tree{}
	for _, treeNode := range node {
		tree = append(tree, &providers.Tree{
			Name: treeNode.Name,
			Path: treeNode.Path,
			Type: treeNode.Type,
		})
	}
	return tree
}
