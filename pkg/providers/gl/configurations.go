package gl

type GitlabProviderConfig struct {
	Token        string        `yaml:"token"`
	Host         string        `yaml:"host"`
	Project      string        `yaml:"project"`
	Commit       *Commit       `yaml:"commit"`
	MergeRequest *MergeRequest `yaml:"mr"`
	Branch       *Branch       `yaml:"branch"`
}

type Commit struct {
	MessagePrefix string `yaml:"messagePrefix"`
	Collect       bool   `yaml:"collect"`
	User          string `yaml:"user"`
	Email         string `yaml:"email"`
}

type MergeRequest struct {
	Title    string `yaml:"title"`
	Reviewer string `yaml:"reviewer"`
	Target   string `yaml:"target"`
	Merge    string `yaml:"merge"`
}

type Branch struct {
	Name string `yaml:"name"`
	From string `yaml:"from"`
	Use  string `yaml:"use"`
}

// TODO: is there a better way??
func CheckConfigurationAndSetStaticDefaults(gpc *GitlabProviderConfig) {
	switch {
	case gpc.Project == "":
		panic("project must be set")
	case gpc.Token == "":
		panic("token must be set")
	case gpc.Branch == nil:
		panic("Branch must be set")
	case gpc.Branch.Use != "" && gpc.Branch.Name != "":
		panic("Branch: Use and Name are exclusive")
	case gpc.Commit.MessagePrefix == "":
		panic("Commit: messagePrefix must be set")
	case gpc.Commit.User == "":
		panic("Commit: user must be set")
	case gpc.Commit.Email == "":
		panic("Commit: email must be set")
	case gpc.MergeRequest != nil:
		switch {
		case gpc.MergeRequest.Title == "":
			panic("mr: title must be set")
		case gpc.MergeRequest.Reviewer == "":
			panic("mr: reviewer must be set")
		case gpc.MergeRequest.Merge == "":
			gpc.MergeRequest.Merge = "never"
		}
	}
}
