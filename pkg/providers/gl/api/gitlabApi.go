package api

import (
	"encoding/base64"
	"fmt"
	"github.com/xanzy/go-gitlab"
	"log"
	"transgit/pkg/providers"
)

type GlApiInterface interface {
	ListTree(pid interface{}, ref string) ([]*gitlab.TreeNode, error)
	GetDefaultBranch(pid interface{}) (*gitlab.Branch, error)
	ListBranches(pid interface{}) ([]*gitlab.Branch, error)
	GetFile(pid interface{}, fileName string, ref string) (*gitlab.File, error)
	CommitNewFiles(pid interface{}, ref, commitMessage, name, email string, actions []*gitlab.CommitActionOptions)
	CommitUpdatedFiles(pid interface{}, ref, commitMessage, name, email string, actions []*gitlab.CommitActionOptions)
	CommitCreatedFile(pid interface{}, ref, commitMessage, name, email string, file *providers.File) error
	CommitUpdatedFile(pid interface{}, ref, commitMessage, name, email string, file *providers.File) error
	NewBranch(pid interface{}, ref string, branch string) (*gitlab.Branch, error)
	MergeMergeRequest(pid interface{}, mergeRequestID int, mergeWhenPipelineSucceeds bool) (*gitlab.MergeRequest, error)
	CreateMergeRequest(pid interface{}, tile, reviewer, srcBranch, targetBranch string) (*gitlab.MergeRequest, error)
	GetUserId(username string) (int, error)
}

type GlApi struct {
	Client *gitlab.Client
}

func (gl *GlApi) GetUserId(username string) (int, error) {
	opts := &gitlab.ListUsersOptions{
		Username: &username,
		Blocked:  gitlab.Bool(false),
		Active:   gitlab.Bool(true),
	}
	users, _, err := gl.Client.Users.ListUsers(opts)
	if err != nil {
		return 0, err
	}
	if len(users) == 0 {
		return 0, fmt.Errorf("found more or none user for this username %s", username)
	}
	return users[0].ID, nil

}

func (gl *GlApi) CreateMergeRequest(pid interface{}, tile, reviewer, srcBranch, targetBranch string) (*gitlab.MergeRequest, error) {
	reviewerId, err := gl.GetUserId(reviewer)
	if err != nil {
		return nil, err
	}
	opts := &gitlab.CreateMergeRequestOptions{
		Title:              &tile,
		Description:        nil,
		SourceBranch:       &srcBranch,
		TargetBranch:       &targetBranch,
		Labels:             nil,
		AssigneeID:         &reviewerId,
		AssigneeIDs:        nil,
		ReviewerIDs:        &[]int{reviewerId},
		TargetProjectID:    nil,
		MilestoneID:        nil,
		RemoveSourceBranch: gitlab.Bool(true),
		Squash:             nil,
		AllowCollaboration: nil,
	}
	mergeRequest, _, err := gl.Client.MergeRequests.CreateMergeRequest(pid, opts)
	if err != nil {
		return nil, err
	}
	return mergeRequest, nil
}

func (gl *GlApi) MergeMergeRequest(pid interface{}, mergeRequestID int, mergeWhenPipelineSucceeds bool) (*gitlab.MergeRequest, error) {
	opts := &gitlab.AcceptMergeRequestOptions{
		MergeWhenPipelineSucceeds: gitlab.Bool(mergeWhenPipelineSucceeds),
	}
	mergeRequest, _, err := gl.Client.MergeRequests.AcceptMergeRequest(pid, mergeRequestID, opts)
	if err != nil {
		return nil, err
	}
	return mergeRequest, nil
}

func (gl *GlApi) NewBranch(pid interface{}, ref string, branch string) (*gitlab.Branch, error) {
	opts := gitlab.CreateBranchOptions{
		Branch: &branch,
		Ref:    &ref,
	}
	branchNew, _, err := gl.Client.Branches.CreateBranch(pid, &opts)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return branchNew, nil

}

func (gl *GlApi) CommitUpdatedFile(pid interface{}, ref, commitMessage, name, email string, file *providers.File) error {
	encodedFile := base64EncodeFile(file)
	fileEncoding := "base64"
	opts := &gitlab.UpdateFileOptions{
		Branch:        &ref,
		CommitMessage: &commitMessage,
		AuthorEmail:   &email,
		AuthorName:    &name,
		Content:       &encodedFile,
		Encoding:      &fileEncoding,
	}
	_, _, err := gl.Client.RepositoryFiles.UpdateFile(pid, file.Path, opts)
	return err
}

func (gl *GlApi) CommitCreatedFile(pid interface{}, ref, commitMessage, name, email string, file *providers.File) error {
	encodedFile := base64EncodeFile(file)
	fileEncoding := "base64"
	opts := &gitlab.CreateFileOptions{
		Branch:        &ref,
		CommitMessage: &commitMessage,
		AuthorEmail:   &email,
		AuthorName:    &name,
		Content:       &encodedFile,
		Encoding:      &fileEncoding,
	}
	_, _, err := gl.Client.RepositoryFiles.CreateFile(pid, file.Path, opts)
	if err != nil {
		fmt.Printf("Error Commiting file: %s", file.Path)
		log.Fatal(err)
	}
	return err
}

func (gl *GlApi) CommitUpdatedFiles(pid interface{}, ref, commitMessage, name, email string, actions []*gitlab.CommitActionOptions) {
	opts := &gitlab.CreateCommitOptions{
		Branch:        &ref,
		CommitMessage: &commitMessage,
		Actions:       actions,
		AuthorEmail:   &email,
		AuthorName:    &name,
	}
	gl.Client.Commits.CreateCommit(pid, opts)
}

func (gl *GlApi) CommitNewFiles(pid interface{}, ref, commitMessage, name, email string, actions []*gitlab.CommitActionOptions) {
	opts := &gitlab.CreateCommitOptions{
		Branch:        &ref,
		CommitMessage: &commitMessage,
		Actions:       actions,
		AuthorEmail:   &email,
		AuthorName:    &name,
	}
	gl.Client.Commits.CreateCommit(pid, opts)
}

func (gl *GlApi) GetFile(pid interface{}, fileName string, ref string) (*gitlab.File, error) {
	opts := gitlab.GetFileOptions{
		Ref: &ref,
	}
	file, _, err := gl.Client.RepositoryFiles.GetFile(pid, fileName, &opts)
	if err != nil {
		return nil, err
	}
	return file, nil

}

func (gl *GlApi) ListBranches(pid interface{}) ([]*gitlab.Branch, error) {
	opts := gitlab.ListBranchesOptions{}
	branches, _, err := gl.Client.Branches.ListBranches(pid, &opts)
	if err != nil {
		return nil, err
	}
	return branches, nil

}

func (gl *GlApi) GetDefaultBranch(pid interface{}) (*gitlab.Branch, error) {
	branches, err := gl.ListBranches(pid)
	if err != nil {
		return nil, err
	}
	for _, b := range branches {
		if b.Default {
			return b, nil
		}
	}
	return nil, fmt.Errorf("No default")
}

func (gl *GlApi) ListTree(pid interface{}, ref string) ([]*gitlab.TreeNode, error) {
	opt := &gitlab.ListTreeOptions{
		Ref:       &ref,
		Recursive: gitlab.Bool(true),
	}
	resultTree := []*gitlab.TreeNode{}
	for {
		tree, resp, err := gl.Client.Repositories.ListTree(pid, opt)
		if err != nil {
			log.Fatal(err)
			return nil, err
		}

		if resultTree != nil {
			resultTree = append(resultTree, tree...)
		} else {
			resultTree = tree
		}

		if resp.NextPage == 0 {
			break
		}
		opt.Page = resp.NextPage
	}
	return resultTree, nil
}

func base64EncodeFile(file *providers.File) string {
	return base64.StdEncoding.EncodeToString([]byte(file.Content))
}
