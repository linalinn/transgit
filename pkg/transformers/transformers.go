package transformers

type TransFormer interface {
	CheckConditions() bool
	ApplyTransformation() error
}
