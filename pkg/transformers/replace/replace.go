package replace

import (
	"regexp"
	"transgit/pkg/providers"
)

type Conditions struct {
	Filename string `yaml:"filename,omitempty"`
}

type ReplaceConfig struct {
	Find       string     `yaml:"find"`
	Replace    string     `yaml:"replace"`
	Conditions Conditions `yaml:"conditions,omitempty"`
}

type Replace struct {
	replaceConfig ReplaceConfig
	provider      providers.ReadOnlyProvider
	find          *regexp.Regexp
	targets       []string
}

func NewReplaceFromConfig(config ReplaceConfig, provider providers.ReadOnlyProvider) (*Replace, error) {
	regex, err := regexp.Compile(config.Find)
	if err != nil {
		return nil, err
	}
	return &Replace{
		replaceConfig: config,
		provider:      provider,
		find:          regex,
		targets:       []string{},
	}, nil

}

func (r *Replace) CheckConditions() bool {
	tree, _ := r.provider.GetTree()
	condition := false
	for _, e := range tree {
		switch {
		case r.replaceConfig.Conditions.Filename != "":
			ok, _ := regexp.MatchString(r.replaceConfig.Conditions.Filename, e.Name)
			if ok {
				r.targets = append(r.targets, e.Path)
				condition = true
			}
		}
	}
	return condition
}

func (r *Replace) ApplyTransformation() error {
	for _, traget := range r.targets {
		file, err := r.provider.GetFile(traget)
		if err != nil {
			return err
		}
		file.Content = r.find.ReplaceAllString(file.Content, r.replaceConfig.Replace)
	}
	return nil
}
