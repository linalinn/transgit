package file

import "transgit/pkg/providers"

type FileConfig struct {
	Name    string `yaml:"name"`
	Path    string `yaml:"path"`
	Content string `yaml:"content"`
	Delete  bool   `yaml:"delete"`
}

type File struct {
	config   FileConfig
	provider providers.Provider
}

func NewFileFromConfig(config FileConfig, provider providers.Provider) *File {
	return &File{
		config:   config,
		provider: provider,
	}

}

func (f *File) CheckConditions() bool {
	// TODO: Maybe check if files that are marked for removal exist
	return true
}

func (f *File) ApplyTransformation() error {
	if f.config.Delete == false {
		f.provider.CreateFile(&providers.File{
			Name:    f.config.Name,
			Path:    f.config.Path,
			Hash:    "",
			Size:    0,
			Content: f.config.Content,
		},
			&providers.CreateFileOptions{})
	}
	return nil
}
