package config

import (
	"gopkg.in/yaml.v3"
	"transgit/pkg/providers/gl"
	"transgit/pkg/transformers/file"
	"transgit/pkg/transformers/replace"
)

type Provider struct {
	Name   string `yaml:"name"`
	Config yaml.Node
}

type Transformations struct {
	Replace []replace.ReplaceConfig `yaml:"replace"`
	File    []file.FileConfig       `yaml:"file"`
}

type TransformationConfig struct {
	Provider        Provider        `yaml:"provider"`
	Transformations Transformations `yaml:"transformations"`
}

func NewProviderByName(name string, config yaml.Node) *gl.GitlabProvider {
	switch name {
	case "gl":
		return gl.NewGitlabProviderFromYamlNode(config)
	}
	return nil
}
