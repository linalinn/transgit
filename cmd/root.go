package cmd

import (
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"transgit/pkg/config"
	"transgit/pkg/providers"
	"transgit/pkg/transformers"
	"transgit/pkg/transformers/file"
	"transgit/pkg/transformers/replace"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "transgit",
	Short: "Run transformations on an git repo",

	RunE: func(cmd *cobra.Command, args []string) error {

		configFilePath, err := cmd.Flags().GetString("config")
		if err != nil {
			log.Fatalf("Failed getting config flag")
			return err
		}

		filename, _ := filepath.Abs(configFilePath)
		yamlFile, err := ioutil.ReadFile(filename)

		if err != nil {
			log.Fatalf("Unmarshal: %v", err)
			return err
		}

		var transformationConfig config.TransformationConfig

		err = yaml.Unmarshal(yamlFile, &transformationConfig)
		if err != nil {
			log.Fatalf("Unmarshal: %v", err)
			return err
		}
		var provider providers.Provider
		provider = config.NewProviderByName(transformationConfig.Provider.Name, transformationConfig.Provider.Config)
		defer provider.Close()

		transformers := []transformers.TransFormer{}

		for _, e := range transformationConfig.Transformations.Replace {
			re, err := replace.NewReplaceFromConfig(e, provider)
			if err != nil {
				log.Fatalf("Faild initalizing Replace: %v", err)
			}
			transformers = append(transformers, re)
		}

		for _, e := range transformationConfig.Transformations.File {
			re := file.NewFileFromConfig(e, provider)
			transformers = append(transformers, re)
		}

		for _, transformer := range transformers {
			transformer.CheckConditions()
		}

		for _, transformer := range transformers {
			err := transformer.ApplyTransformation()
			if err != nil {
				log.Fatalf("Filed to apply Transformation: %v", err)
				return err
			}
		}

		provider.CommitAll()

		return nil
	},
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {

	rootCmd.PersistentFlags().String("config", "", "Path to config file")
	rootCmd.MarkFlagRequired("config")
}
